var contactMeService = (function () {
    const re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    function ContactMeService() {}

    ContactMeService.prototype.validateEmail = function (email) {
        return (re.test(String(email).toLowerCase())) ? true : false;
    }

    return new ContactMeService();
})();