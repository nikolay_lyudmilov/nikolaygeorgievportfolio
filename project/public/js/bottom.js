// $('.menuBtnNav').click(function () {
//     $('#nav-in-header>ul').toggleClass('activeNav');
// })

$('.ohw-menu').owlCarousel({
    items: 1,
    loop: true,
    autoplay: false,
    autoHeight: true,
    responsive: {
        678: {
            mergeFit: true
        },
        1000: {
            mergeFit: false
        }
    }
});

var loader;

function loadNow(opacity) {
    if (opacity <= 0) {
        displayContent();
    } else {
        loader.style.opacity = opacity;
        window.setTimeout(function () {
            loadNow(opacity - 0.02);
        }, 40);
    }
}

function displayContent() {
    loader.style.display = 'none';
    document.getElementById('all_content').style.display = "block";
}

loader = document.getElementById('loader');
loadNow(1);