var express = require('express');
var router = express.Router();
var sha1 = require('sha1');

router.post('/', function (req, res, next) {
    var username = req.body.username;
    var password = sha1(req.body.password);

    var userCollection = req.db.get('users');
    userCollection.find({
        username: username,
        password: password
    }, {}, function (err, docs) {
        if (docs.length > 0) {
            res.status(200);
            res.redirect('http://localhost:3000/#game');
        } else {
            res.status(401);
            res.redirect('http://localhost:3000/#login');

        }
    })
});

module.exports = router;