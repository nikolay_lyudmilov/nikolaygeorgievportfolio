var weatherStorage = (function () {

    function WeatherStorage() {
        this.weathers = [];
    }

    WeatherStorage.prototype.getWeather = function (city) {
        var self = this;
        return new Promise((resolve, reject) => {
            const HTTP_OK_START = 200,
                HTTP_OK_END = 300;
            var xhr, api = 'http://api.openweathermap.org/data/2.5/weather?q=',
                apiKey = '&apikey=f631fd357c75163a46154773a513dd64';

            (XMLHttpRequest) ? xhr = new XMLHttpRequest(): xhr = new ActiveXObject();

            xhr.open('GET', `${api}${city}${apiKey}`, true);
            xhr.send(null);

            xhr.addEventListener('load', () => {
                if ((xhr.status >= HTTP_OK_START) && (xhr.status < HTTP_OK_END)) {
                    var data = JSON.parse(xhr.responseText);
                    self.weathers.push(data);
                    resolve(data);
                } else {
                    reject(xhr.status);
                }
            })
        });
    }

    WeatherStorage.prototype.getWeatherFromStorage = function (city) {
        return this.weathers.find(w => w.name == city);
    }

    WeatherStorage.prototype.setIcons = function (icon, main) {
        var skycons = new Skycons({
                color: '#fff'
            }),
            currentIcon = 'CLEAR_NIGHT';

        var day = false;
        var date = new Date();
        var timestamp = date.getTime();
        timestamp = date.toString();
        var hour = timestamp.split(" ")[4].split(":")[0];
        (hour.charAt(0) == 0) ? +hour.split('')[1]: +hour;
        if ((+hour < 19) && (+hour > 7)) {
            day = true;
        } else {
            day = false;
        }
        
        if (!!day) {
            switch (main) {
                case "Clear":
                    currentIcon = "CLEAR_DAY"
                    break;
                case "Clouds":
                    currentIcon = "PARTLY_CLOUDY_DAY"
                    break;
            }

        } else {
            switch (main) {
                case "Clear":
                    currentIcon = "CLEAR_NIGHT"
                    break;
                case "Clouds":
                    currentIcon = "PARTLY_CLOUDY_NIGHT"
                    break;
            }
        }

        switch (main) {
            // CLEAR_DAY 
            // CLEAR_NIGHT
            // PARTLY_CLOUDY_DAY
            // PARTLY_CLOUDY_NIGHT
            // CLOUDY
            // RAIN
            // SLEET
            // SNOW
            // WIND
            // FOG

            case "Thunderstorm":
                currentIcon = "SLEET"
                break;
            case "Drizzle":
                currentIcon = "RAIN";
                break;
            case "Rain":
                currentIcon = "RAIN";
                break;
            case "Snow":
                currentIcon = "SNOW";
                break;
            case "Mist":
                currentIcon = "FOG";
                break;
            case "Smoke":
                currentIcon = "FOG"
                break;
            case "Haze":
                currentIcon = "FOG"
                break;
            case "Dust":
                currentIcon = "FOG"
                break;
            case "Fog":
                currentIcon = "FOG"
                break;
            case "Sand":
                currentIcon = "FOG"
                break;
            case "Squall":
                currentIcon = "FOG"
                break;
            case "Tornado":
                currentIcon = "FOG"
                break;
        }

        skycons.play();
        return skycons.set(icon, Skycons[currentIcon]);
    }

    return new WeatherStorage();


})();